//JQUERY SELECTORS
var derButton, factsButton, labButton, links,
	menuFrame, frames, model, modelContent;

//SOUNDS
var audio = [];

//TIMEOUTS
var timeout = [];

//OTHER GLOBAL DATA
var shots, currentShot;

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
	
	var currPiece = this,
		currAudio = this.audio;
	
	currAudio.addEventListener("timeupdate", function(){
		var currTime = Math.round(currAudio.currentTime);
		
		if(currTime == currPiece.end)
			currAudio.pause();
	});
}

AudioPiece.prototype.ended = function(callBack)
{
	this.audio.addEventListener("ended", callBack);
};

AudioPiece.prototype.play = function()
{
	this.audio.currentTime = this.start;
	this.audio.play();
}

AudioPiece.prototype.pause = function()
{
	this.audio.pause();
}

AudioPiece.prototype.addEventListener = function(e, callBack)
{
	var currPiece = this,
		currAudio = this.audio,
		currDuration = Math.round(currAudio.duration);
	
	if(e === "ended")
	{
		console.log("else end: " + currPiece.end + ", " + currDuration);
		currAudio.addEventListener("timeupdate", function(){
			var currTime = Math.round(currAudio.currentTime);
				
			if(currTime === currPiece.end)
			{
				currAudio.pause();
				currAudio.currentTime += 3;
				callBack();
			}
		});
	}
	if(e === "timeupdate")
	{
		currPiece.audio.addEventListener("timeupdate", callBack);
	}
}

AudioPiece.prototype.getStart = function()
{
	return this.start;
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum),
			preLoadImage = $("#pre-load .pre-load-image");
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			preLoadImage.css("width", loadPercentage + "%");
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

var createDragTask = function(prefix, draggables, finishFunction){
	var draggabillies = [],
		vegetable, basket;
	
	for (var i = 0; i < draggables.length; i ++)
		draggabillies[i] = new Draggabilly(draggables[i]);
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target); 
		vegetable.css("background-color", "#5AA0F5");
	}
	
	var onEnd = function(instance, event, pointer){
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(vegetable.attr("data-key") === basket.attr("data-key"))
		{
			vegetable.remove();
			basket.css("background-color", vegetable.css("background-color"));
			basket.html(vegetable.html());

			if(!$(prefix + ".ball").length)
			{
				finishFunction();
			}
		}
		else
		{
			vegetable.css("left", "");
			vegetable.css("top", "");
			vegetable.css("background-color", "");
			vegetable.fadeIn(0);
		}
	}
	
	for (var i = 0; i < draggabillies.length; i ++)
	{
		draggabillies[i].on("dragStart", onStart);
		draggabillies[i].on("dragEnd", onEnd);
	}
}

var launch101 = function(){
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var prefix = "#" + theFrame.attr("id") + " ",
		guysPlaying = $(prefix + ".guys-playing"), 
		guysPlayingAnimated = $(prefix + ".guys-playing-animated"), 
		clock = $(prefix + ".clock"),
		firework = $(prefix + ".firework"),
		cloud = $(prefix + ".cloud"),
		newPlace = $(prefix + " .new-place"),
		
	
	clockSprite = new Motio(clock[0], {
		fps: 1/5,
		frames: 3
	});
	
	clockSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			audio[2].play();
			firework.fadeOut(0);
			guysPlaying.fadeIn();
			guysPlayingAnimated.fadeOut(0);
			timeout[0] = setTimeout(function(){
				cloud.fadeIn(3000);
			}, 1000);
			timeout[1] = setTimeout(function(){
				newPlace.addClass("transition-3s");
				timeout[2] = setTimeout(function(){
					newPlace.css("opacity", "1");
					audio[3].play();
					timeout[3] = setTimeout(function(){
						fadeNavsIn();
					}, 4000);
				}, 2000); 
			}, 4000);
		}
	});
	
	audio[0] = new Audio("audio/firework.mp3");
	audio[1] = new Audio("audio/clock-tick.mp3");
	audio[2] = new Audio("audio/chimes.mp3");
	audio[3] = new Audio("audio/magic.mp3");
	
	audio[0].addEventListener("ended", function(){
		firework.fadeOut(0);
	});
	
	guysPlayingAnimated.fadeOut(0);
	firework.fadeOut(0);
	cloud.fadeOut(0);
	fadeNavsIn();
	newPlace.css("opacity", "0");
	
	startButtonListener = function(){
		audio[0].play();
		audio[1].play();
		firework.fadeIn(0);
		clockSprite.play();
		guysPlaying.fadeOut(0);
		guysPlayingAnimated.fadeIn(0);
	};
}

var launch102 = function(){
		theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boysBack = $(prefix + ".boys-back"),
		boysWalking = $(prefix + ".boys-walking"),
		bg = $(prefix + ".bg-base");
	
	var boysWalkingSprite = new Motio(boysWalking[0], {
		fps: 3,
		frames: 7
	});
	
	boysWalkingSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			boysWalkingSprite.pause();
	});
	
	boysWalking.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/footsteps.mp3");
	audio[0].addEventListener("ended", function(){
		fadeNavsIn();
	});
	
	startButtonListener = function(){
		bg.addClass("bg-base-2");
		timeout[0] = setTimeout(function(){
			boysBack.fadeOut(0);
			bg.fadeOut(0);
			boysWalking.fadeIn(0);
			theFrame.css("background-image", "url('pics/debereiner.png')");
			boysWalkingSprite.play();
			audio[0].play();
		}, 4000);
	};
}

var launch103 = function(){
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		boysBack = $(prefix + ".boys-back"),
		debereinerWriting = $(prefix + ".debereiner-writing"),
		debereinerSpeaking = $(prefix + ".debereiner-speaking"),
		debereinerIdle = $(prefix + ".debereiner-idle");
	
	audio[0] = new Audio("audio/s5-1.mp3");
	audio[1] = new Audio("audio/s6-1.mp3");
		
	audio[0].addEventListener("ended", function(){
		debereinerWriting.fadeOut(0);
		debereinerSpeaking.fadeIn(0);
		audio[1].play();
	});
	
	audio[1].addEventListener("ended", function(){
		debereinerSpeaking.fadeOut(0);
		debereinerIdle.fadeIn(0);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 3000);
	});
	
	boysBack.css("top", "100%");
	debereinerSpeaking.fadeOut(0);
	debereinerIdle.fadeOut(0);
	fadeNavsIn();
		
	startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		boysBack.animate({
			"top": "0%"
		}, 1000, function(){
			audio[0].play();
		});
	};
}

var launch104 = function(){
		theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyAsking = $(prefix + ".boy-asking"),
		boysIdle = $(prefix + ".boys-idle"),
		debereinerIdle = $(prefix + ".debereiner-idle"),
		debereinerSpeaking = $(prefix + ".debereiner-speaking"),
		year1829 = $(prefix + ".year-1829");
	
	boyAsking.fadeOut(0);
	debereinerSpeaking.fadeOut(0);
	year1829.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/s7-1.mp3");
	audio[1] = new Audio("audio/s8-1.mp3");
	audio[2] = new Audio("audio/s9-1.mp3");
	audio[3] = new Audio("audio/s10-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		boysIdle.fadeIn(0);
		boyAsking.fadeOut(0);
		timeout[0] = setTimeout(function(){
			audio[1].play();
			debereinerIdle.fadeOut(0);
			debereinerSpeaking.fadeIn(0);
			year1829.fadeIn("slow");
			year1829.animate({
				"left": "-150%"
			},5000, function(){
				year1829.fadeOut("slow");
			});
		}, 1000);
	});
	audio[1].addEventListener("ended", function(){
		debereinerIdle.fadeIn(0);
		debereinerSpeaking.fadeOut(0);
		timeout[1] = setTimeout(function(){
			boysIdle.fadeOut(0);
			boyAsking.fadeIn(0);
			audio[2].play();
		}, 1000);
	});
	
	audio[2].addEventListener("ended", function(){
		boysIdle.fadeIn(0);
		boyAsking.fadeOut(0);
		timeout[2] = setTimeout(function(){
			audio[3].play();
			debereinerIdle.fadeOut(0);
			debereinerSpeaking.fadeIn(0);
		}, 1000);
	});
	
	audio[3].addEventListener("ended", function(){
		debereinerIdle.fadeIn(0);
		debereinerSpeaking.fadeOut(0);
		fadeNavsIn();
	});
	
	startButtonListener = function(){
		audio[0].play();
		boyAsking.fadeIn(0);
		boysIdle.fadeOut(0);
	};
}

var launch105 = function(){
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		fields = $(prefix + ".field"),
		checkButton = $(prefix + ".check-button");
	
	audio[0] = new Audio("audio/s11-1.mp3");
	
	checkButton.fadeOut(0);
	fadeNavsIn()
		
	startButtonListener = function(){
		audio[0].play();
	};
		
	var fieldListener = function(){
		if(fields[0].value)
			if(fields[1].value)
				if(fields[2].value)
					checkButton.fadeIn(0);
	}
	
	fields.off("keyup", fieldListener);
	fields.on("keyup", fieldListener);
	for(var i = 0; i < fields.length; i++)
		$(fields[i]).val("");
	
	var checkButtonListener = function(){
		correctAnswers = ["23.0195","88.705","76.3619"];
		for(var i = 0; i < fields.length; i++)
			if(fields[i].value === correctAnswers[i])
				$(fields[i]).css("color", "green");
			else
				$(fields[i]).css("color", "red");
			
		timeout[0] = setTimeout(function(){
			for(var i = 0; i < fields.length; i++)
			{
				$(fields[i]).css("color", "green");
				$(fields[i]).val(correctAnswers[i]);
			}
		}, 4000);
	}
	
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	var draggables = $(prefix + ".ball");
	var draggabillies = [],
		vegetable, basket;
	
	for (var i = 0; i < draggables.length; i ++)
		draggabillies[i] = new Draggabilly(draggables[i]);
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target);
	}
	
	var onEnd = function(instance, event, pointer){
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(vegetable.attr("data-key") === basket.attr("data-key"))
		{
			vegetable.remove();
			var bgImage = "url('pics/" + basket.attr("data-key") + "-green.png')";
			basket.css("background-image", bgImage);
			if(!$(prefix + ".ball").length)
			{
				fadeNavsIn();
			}
		}
		else
		{
			vegetable.css("left", "");
			vegetable.css("top", "");
			vegetable.fadeIn(0);
		}
	}
	
	for (var i = 0; i < draggabillies.length; i ++)
	{
		draggabillies[i].on("dragStart", onStart);
		draggabillies[i].on("dragEnd", onEnd);
	}
}

var launch106 = function(){
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyMouth = $(prefix + ".boy-mouth");
	
	audio[0] = new Audio("audio/s12-1.mp3");
	
	fadeNavsIn();
	boyMouth.fadeOut(0);
	
	audio[0].addEventListener("ended", function(){
		fadeNavsIn();
		boyMouth.fadeOut(0);
	});
	
	startButtonListener = function(){
		audio[0].play();
		boyMouth.fadeIn(0);
	}
}

var launch107 = function(){
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boysWalking = $(prefix + ".boys-walking-left");
	
	audio[0] = new Audio("audio/footsteps.mp3");
		
	var boysSprite = new Motio(boysWalking[0], {
		fps: 3,
		frames: 7
	});
	
	boysSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			fadeNavsIn();
		}
	});
	
	fadeNavsIn();
		
	startButtonListener = function(){
		audio[0].play();
		boysSprite.play();
	};
}

var launch108 = function(){
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boysIdle = $(prefix + ".boys-idle"),
		boysSpeaking = $(prefix + ".boys-speaking"),
		reinaIdle = $(prefix + ".reina-idle"), 
		reinaSpeaking = $(prefix + ".reina-speaking"),
		year1865 = $(prefix + ".year-1865"),
		tableTask = $(prefix + ".table-task"),
		piano = $(prefix + ".piano"),
		table2 = $(prefix + ".table-2"),
		balls = $(prefix + ".ball"),
		baskets = $(prefix + ".basket"),
		checkButton = $(prefix + ".check-button");
	
	var reinaSprite = new Motio(reinaIdle[0], {
		fps: 1, 
		frames: 2
	});
	
	var pianoSprite = new Motio(piano[0], {
		fps: 2,
		frames: 14
	});
	
	pianoSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			timeout[4] = setTimeout(function(){
				piano.fadeOut(0);
			}, 2000);
		}
	});
	
	audio[0] = new Audio("audio/s15-1.mp3");
	audio[1] = new Audio("audio/s16-1.mp3");
			
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 5 && currTime != doneSecond)
		{
			doneSecond = currTime;
			year1865.fadeIn(500);
			year1865.animate({
				"left": "150%"
			}, 5000, function(){
				year1865.fadeOut(0);
			});
		}
	});
	
	audio[0].addEventListener("ended", function(){
		reinaIdle.fadeIn(0);
		reinaSpeaking.fadeOut(0);
		timeout[3] = setTimeout(function(){
			tableTask.fadeIn(500);
			balls.fadeIn(500);
			baskets.fadeIn(500);
		}, 2000);
	});
	
	audio[1].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 5 && currTime != doneSecond)
		{
			doneSecond = currTime;
			piano.fadeIn(0);
			pianoSprite.play();
		}
		else if(currTime === 10 && currTime != doneSecond)
		{
			doneSecond = currTime;
			table2.fadeIn(500);
		}	
		else if(currTime === 30 && currTime != doneSecond)
		{
			doneSecond = currTime;
			table2.fadeOut(0);
		}	
		else if(currTime === 38 && currTime != doneSecond)
		{
			currAudio.pause();
			doneSecond = currTime;
			reinaIdle.fadeIn(0);
			reinaSpeaking.fadeOut(0);
			timeout[1] = setTimeout(function(){
				fadeNavsIn();
			}, 1000);
		}			
	});
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		basket.attr("data-color", "#02A2E2");
		vegetable.remove();
	};
	
	var failFunction = function(vegetable, basket){
		if(basket.hasClass("basket"))
		{
			basket.html(vegetable.html());
			basket.attr("data-color", "#EA361A");
			vegetable.remove();
		}
	};
	
	var finishCondition = function(){
		return $(prefix + ".ball").length <= 1;
	};
	
	var finishFunction = function(){
		checkButton.fadeIn(0);
	};
	
	var dragTask = DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction)
	
	fadeNavsIn();
	boysSpeaking.fadeOut(0);
	reinaSpeaking.fadeOut(0);
	year1865.fadeOut(0);
	tableTask.fadeOut(0);
	piano.fadeOut(0);
	table2.fadeOut(0);
	checkButton.fadeOut(0);
	balls.fadeOut(0);
	baskets.fadeOut(0);
	
	var checkButtonListener = function(){
		for(var i = 0; i < baskets.length; i++)
			$(baskets[i]).css("background-color", $(baskets[i]).attr("data-color"));
		timeout[4] = setTimeout(function(){
			tableTask.fadeOut(0);
			baskets.fadeOut(0);
			balls.fadeOut(0);
			checkButton.fadeOut(0);
			reinaIdle.fadeOut(0);
			reinaSpeaking.fadeIn(0);
			audio[1].play();
		}, 6000);
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			reinaSprite.to(1, true);
		}, 1000);
		timeout[1] = setTimeout(function(){
			audio[0].play();
			reinaSpeaking.fadeIn(0);
		}, 3000);
	};
}

var launch109 = function(){
		theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		manMouth = $(prefix + ".man-mouth"),
		boyMouth = $(prefix + ".boy-mouth");
	
	audio[0] = new AudioPiece("s16-1", 37, 99);
	audio[1] = new Audio("audio/s17-1.mp3");
		
	audio[0].ended(function(){
		manMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			boyMouth.fadeIn(0);
			audio[1].play();
		}, 1000);
	});
	
	audio[1].addEventListener("ended", function(){
		boyMouth.fadeOut(0);
		fadeNavsIn();
	});
	
	fadeNavsIn();
	manMouth.fadeOut(0);
	boyMouth.fadeOut(0);
		
	startButtonListener = function(){
		manMouth.fadeIn(0);
		audio[0].play();
	};
}

var launch110 = function(){
		theFrame = $("#frame-110"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base");
	
	fadeNavsIn();
		
	startButtonListener = function(){
		bg.addClass("bg-base-2");
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 3000);
	};
}

var launch111 = function(){
		theFrame = $("#frame-111"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boysWalking = $(prefix + ".boys-walking-left");
	
	var boysWalkingSprite = new Motio(boysWalking[0], {
		fps: 4,
		frames: 7
	});
	
	boysWalkingSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	audio[0] = new Audio("audio/footsteps.mp3");
		
	audio[0].addEventListener("ended", function(){
		fadeNavsIn();
	});
	
	fadeNavsIn();
		
	startButtonListener = function(){
		audio[0].play();
		boysWalkingSprite.play();
	};
}

var launch112 = function(){
		theFrame = $("#frame-112"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		mendeleevTableShadow = $(prefix + ".mendeleev-table-shadow"), 
		mendeleevIdle = $(prefix + ".mendeleev-idle"), 
		mendeleevSmiling = $(prefix + ".mendeleev-smiling"), 
		mendeleevSpeaking = $(prefix + ".mendeleev-speaking"), 
		crowd = $(prefix + ".crowd"), 
		boysBack = $(prefix + ".boys-back"),
		boysRaiseHand = $(prefix + ".boys-raise-hand");
	
	var crowdSprite = new Motio(crowd[0], {
		fps: 3, 
		frames: 5
	});
	
	crowdSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			audio[0].play();
			mendeleevIdle.fadeOut(0);
			mendeleevSmiling.fadeIn(0);
		}
	});
	
	mendeleevSmiling.fadeOut(0);
	mendeleevSpeaking.fadeOut(0);
	boysRaiseHand.fadeOut(0);
	mendeleevTableShadow.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new AudioPiece("s20-1", 0, 5);
	audio[1] = new AudioPiece("s20-1", 4, 32);
	audio[2] = new AudioPiece("s20-1", 32, 55);
		
	audio[0].addEventListener("ended", function(){
		crowdSprite.to(0, true);
		audio[1].play();
		timeout[0] = setTimeout(function(){
			mendeleevSmiling.fadeOut(0);
			mendeleevSpeaking.fadeIn(0);
			mendeleevTableShadow.fadeIn(0);
		}, 1000);
	});
	audio[1].addEventListener("ended", function(){
		audio[2].play();
		mendeleevSmiling.fadeIn(0);
		mendeleevSpeaking.fadeOut(0);
		boysRaiseHand.fadeIn(0);
		boysBack.fadeOut(0);
	});
	audio[2].addEventListener("ended", function(){
		timeout[1] = setTimeout(function(){
			fadeNavsIn();
		}, 1000);
	});
			
	startButtonListener = function(){
		crowdSprite.play();
	};
}

var launch113 = function(){
		theFrame = $("#frame-113"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		manMouth = $(prefix + ".man-mouth"),
		boyMouth = $(prefix + ".boy-mouth"),
		preTablet = $(prefix + ".pre-tablet"),
		tabletShowing = $(prefix + ".tablet-showing"),
		tablet = $(prefix + ".tablet"),
		tabletHand = $(prefix + ".tablet-hand"),
		tabletHighlight = $(prefix + ".tablet-highlight"),
		castle = $(prefix + ".castle"),
		castleElements = $(prefix + ".castle-elements"),
		castleMetals = $(prefix + ".castle-metals"),
		castleNonMetals = $(prefix + ".castle-non-metals"),
		castleGroup1 = $(prefix + ".castle-group-1"),
		castleBoys = $(prefix + ".castle-boys");
	
	audio[0] = new AudioPiece("s20-1", 55,64);
	audio[1] = new AudioPiece("s21-1", 0, 5);
	audio[2] = new AudioPiece("s21-1", 4, 20);
	
	audio[3] = new AudioPiece("s21-1", 20, 65);
		
	audio[0].ended(function(){
		audio[1].play();
		preTablet.fadeIn(0);
		manMouth.fadeIn(0);
		tablet.fadeOut(0);
		tabletHand.fadeOut(0);
		tabletHighlight.fadeOut(0);
		tabletShowing.fadeOut(0);
		preTabletSpeaking.fadeIn(0);
	});
	audio[1].addEventListener("ended", function(){
		audio[2].play();
		boyMouth.fadeIn(0);
		manMouth.fadeOut(0);
	});
	audio[2].addEventListener("ended", function(){
		boyMouth.fadeOut(0);
		manMouth.fadeIn(0);
		audio[3].play();
		castle.fadeIn(0);
		castleBoys.fadeIn(0);
	});
	audio[3].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime), 
			doneSecond = 0;
		
		if(currTime === (audio[3].getStart() + 5) && currTime != doneSecond)
		{
			doneSecond = currTime;
			castleElements.fadeIn(0);
		}
		else if(currTime === (audio[3].getStart() + 15) && currTime != doneSecond)
		{
			doneSecond = currTime;
			castleElements.fadeOut(0);
			castleMetals.fadeIn(0);
		}
		else if(currTime === (audio[3].getStart() + 20) && currTime != doneSecond)
		{
			doneSecond = currTime;
			castleMetals.fadeOut(0);
			castleNonMetals.fadeIn(0);
		}
		else if(currTime === (audio[3].getStart() + 30) && currTime != doneSecond)
		{
			doneSecond = currTime;
			castleNonMetals.fadeOut(0);
			castleGroup1.fadeIn(0);
		}
		else if(currTime === (audio[3].getStart() + 35) && currTime != doneSecond)
		{
			doneSecond = currTime;
			castleGroup1.fadeOut(0);
			castleElements.fadeIn(0);
			timeout[1] = setTimeout(function(){
				fadeNavsIn();
			}, 2000);
		}
	});
	
	tabletShowing.fadeOut(0);
	tablet.fadeOut(0);
	tabletHand.fadeOut(0);	
	tabletHighlight.fadeOut(0);
	castle.fadeOut(0);
	castleElements.fadeOut(0);
	castleMetals.fadeOut(0);
	castleNonMetals.fadeOut(0);
	castleGroup1.fadeOut(0);
	castleBoys.fadeOut(0);
	boyMouth.fadeOut(0);
	manMouth.fadeOut(0);
	fadeNavsIn();
		
	startButtonListener = function(){
		preTablet.fadeOut(0);
		audio[0].play();
		tabletShowing.fadeIn(0);
		timeout[0] = setTimeout(function(){
			tablet.fadeIn(0);
			tabletHand.fadeIn(0);
			tabletHighlight.fadeIn(0);
		}, 1000);
	};
}

var launch113a = function(){
	theFrame = $("#frame-113-a"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		tasks = $(prefix + ".task"),
		activeTask = 1,
		balls = $(prefix + ".ball"),
		checkButtons = $(prefix + ".check-button");
	
	fadeNavsIn();
	tasks.fadeOut(0);
	checkButtons.fadeOut(0);
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		basket.attr("data-color", "green");
		basket.css("border", "none");
		vegetable.remove();
	};
	
	var failFunction = function(vegetable, basket){
		if(basket.hasClass("basket"))
		{
			basket.html(vegetable.html());
			basket.attr("data-color", "red");
			vegetable.remove();
			basket.css("border", "none");
		}
	};
	
	var finishCondition = function(){
		return !$(prefix + ".task-" + activeTask + " .ball").length;
	};
	
	var finishFunction = function(){
		$(prefix + ".task-" + activeTask + " .check-button").fadeIn(0);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction)
	
	var	checkButtonsListener = function(){
		var baskets = $(prefix + ".task-" + activeTask + " span");
		
		for(var i = 0; i < baskets.length; i++)
			$(baskets[i]).css("color", $(baskets[i]).attr("data-color"));
		
		timeout[0] = setTimeout(function(){
			activeTask ++;
			tasks.fadeOut(0);
			if(activeTask < 3)
				$(prefix + ".task-" + activeTask).fadeIn(500);
			else
				fadeNavsIn();
		}, 6000);
	};
	checkButtons.off("click", checkButtonsListener);
	checkButtons.on("click", checkButtonsListener);
	
	startButtonListener = function(){
		$(prefix + ".task-" + activeTask).fadeIn(0);
		$(prefix + ".task-" + activeTask + " .check-button").fadeIn(0);
	};
};

var launch114 = function(){
		theFrame = $("#frame-114"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		darkTableHighlight = $(prefix + ".dark-table-highlight"),
		darkTableHighlight2 = $(prefix + ".dark-table-highlight-2");
		
	audio[0] = new Audio("audio/s24-1.mp3");
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime), 
			doneSecond = 0;
		
		if(currTime === 4 && currTime != doneSecond)
		{
			doneSecond = currTime;
			darkTableHighlight.fadeOut(0);
			darkTableHighlight2.fadeIn(0);
		}
	});
	
	audio[0].addEventListener("ended", function(){
		darkTableHighlight2.fadeOut(0);
		fadeNavsIn();
	});
	
	fadeNavsIn();
	darkTableHighlight.fadeOut(0);
	darkTableHighlight2.fadeOut(0);
		
	startButtonListener = function(){
		audio[0].play();
		timeout[0] = setTimeout(function(){
			darkTableHighlight.fadeIn(0);
		}, 1000);
	};
}

var launch115 = function(){
		theFrame = $("#frame-115"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		showingTable = $(prefix + ".showing-table"),
		presentingTablet = $(prefix + ".presenting-tablet"), 
		boyMouth = $(prefix + ".boy-mouth");
	
	var presentingSprite = new Motio(presentingTablet[0], {
		fps: 3, 
		frames: 4
	});	
	
	presentingSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	audio[0] = new Audio("audio/s25-1.mp3");
			
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 13 && currTime != doneSecond)
		{
			doneSecond = currTime;
			currAudio.pause();
			fadeNavsIn();
		}		
		
		if(currTime === 7 && currTime != doneSecond)
		{
			doneSecond = currTime;
			showingTable.fadeOut(0);
			boyMouth.fadeOut(0);
			presentingTablet.fadeIn(0);
			presentingSprite.play();
		}
	});
	
	fadeNavsIn();
	presentingTablet.fadeOut(0);
	boyMouth.fadeOut(0);
		
	startButtonListener = function(){
		audio[0].play();
		boyMouth.fadeIn(0);
	};
}

var launch116 = function(){
		theFrame = $("#frame-116"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		givingFive = $(prefix + ".giving-five"),
		manMouth = $(prefix + ".man-mouth");
	
	var givingFiveSprite = new Motio(givingFive[0], {
		fps: 3, 
		frames: 4
	});
	
	givingFiveSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			givingFiveSprite.to(0, true);
			audio[0].play();
			manMouth.fadeIn(0);
		}
		
	});
	
	audio[0] = new AudioPiece("s25-1", 12, 23);
		
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === audio[0].getStart() + 8 && currTime != doneSecond)
		{
			manMouth.fadeOut(0);
			doneSecond = currTime;
			fadeNavsIn();
		}
		
	});
	
	fadeNavsIn();
	manMouth.fadeOut(0);
		
	startButtonListener = function(){
		givingFiveSprite.play();
	};
}

var launch301 = function(){
		theFrame = $("#frame-301"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		allTasks = $(prefix + ".task"), 
		answers = $(prefix + ".answer"),
		checkButtons = $(prefix + ".check-button"), 
		touchElement = $(prefix + ".touch-element"), 
		activeTask = 1,
		result = $(prefix + ".task-result"), 
		pointsLabel = $(prefix + ".points-label"),
		points = 0,
		taskPrefix = prefix + ".task-" + activeTask;
		flips = $(prefix + ".flip"), 
		textfields = $(prefix + ".textfield"),
		firework = $(prefix + ".firework"),
		flipSprites = [],
		jeopardyLabel = $(prefix + ".jeopardy-label"),
		jeopardySprite = new Motio(jeopardyLabel[0], {
			fps: 2,
			frames: 5
		});
		jeopardySprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
		});
		
	audio[0] = new Audio("audio/jeopardy-theme.mp3");
	audio[1] = new Audio("audio/jeopardy.mp3");
	audio[2] = new Audio("audio/firework.mp3");
	
	var startQuestion = function()
	{
		if(activeTask <= 8)
			timeout[0] = setTimeout(function(){
				touchElement.fadeIn(0);
				allTasks.fadeOut(0);
				taskPrefix = prefix + ".task-" + activeTask;
				var elementButton = $(taskPrefix + ".element-button");
				$(taskPrefix + ".wink").fadeIn(0);
				elementButton.fadeIn(0);
				elementButton.click(function(){
					touchElement.fadeOut(0);
					$(taskPrefix + ".wink").fadeOut(0);
					$(flips[activeTask - 1]).fadeIn(0);
					flipSprites[activeTask - 1].play();
					$(taskPrefix + ".question").fadeIn(0);
					$(taskPrefix + ".answer").fadeIn(0);
					$(taskPrefix + ".textfield").fadeIn(0);
					$(taskPrefix + ".ball").fadeIn(0);
					$(taskPrefix + ".basket").fadeIn(0);
					if($(taskPrefix + ".ball").length)
						createDragTask(taskPrefix, $(taskPrefix + ".ball"), function(){
							points += 10;
							pointsLabel.html(points);
							pointsLabel.addClass("box-shadow-white");
							timeout[0] = setTimeout(function(){
								pointsLabel.removeClass("box-shadow-white");
							}, 2000);
							activeTask ++;
							startQuestion();
						});
					$(taskPrefix + ".check-button").fadeIn(0);
					$(taskPrefix + ".check-basket-button").fadeIn(0);
				});
			}, 2000);
		else
		{
			timeout[0] = setTimeout(function(){
				allTasks.fadeOut(0);
				result.fadeIn(0);
				result.html("ВЫ НАБРАЛИ " + points + " ОЧКОВ");
				firework.fadeIn(500);
				audio[2].play();
				timeout[0] = setTimeout(function(){
					firework.fadeOut(0);
					audio[2].pause();
				}, 2000);
			}, 2000);
		}
	}

	var answersListener = function(){
		var currAnswer = $(this);
		if(currAnswer.attr("data-correct"))
		{
			points += 10;
			pointsLabel.html(points);
			pointsLabel.addClass("box-shadow-white");
			timeout[0] = setTimeout(function(){
				pointsLabel.removeClass("box-shadow-white");
			}, 2000);
			
			currAnswer.css("color", "green");
		}
		else
		{
			currAnswer.css("color", "red");
		}
		activeTask ++;
		startQuestion();
	};
	answers.off("click", answersListener);
	answers.on("click", answersListener);
	
	var checkButtonListener = function(){
		taskPrefix = prefix + ".task-" + activeTask;
		textfield = $(taskPrefix + ".textfield");
		if(textfield.attr("data-correct").indexOf(textfield.val()) > -1)
		{
			points += 10;
			pointsLabel.html(points);
			pointsLabel.addClass("box-shadow-white");
			timeout[0] = setTimeout(function(){
				pointsLabel.removeClass("box-shadow-white");
			}, 2000);
			
			textfield.css("background-color", "green");
		}
		else
		{
			textfield.css("background-color", "red");
		}
		activeTask ++;
		startQuestion();
	}
	checkButtons
	checkButtons.off("click", checkButtonListener);
	checkButtons.on("click", checkButtonListener);
	textfields.val("");

	for(var i = 0; i < flips.length; i ++)
	{
		flipSprites[i] = new Motio(flips[i], {
			fps: 3,
			frames: 4
		});
		flipSprites[i].on("frame", function(){
			if(this.frame === this.frames - 1)
				this.pause();
		})
	}
	
	jeopardyLabel.fadeOut(0);
	allTasks.fadeOut(0);
	touchElement.fadeOut(0);
	fadeNavsIn();
	firework.fadeOut(0);
	
	startButtonListener = function()
	{
		jeopardyLabel.fadeIn(0);
		timeout[0] = setTimeout(function(){
			audio[1].play();
			audio[0].play();
			jeopardySprite.play();
			timeout[1] = setTimeout(function(){
				startQuestion();
			}, 5000);
		}, 1000);
	};
}

var hideEverythingBut = function(elem)
{
	frames = $(".frame");
	frames.fadeOut(0);
	elem.fadeIn(0);
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	

	for (var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);

	switch(elem.attr("id"))
	{
		case "frame-000":
			fadeNavsOut();
			break;
		case "frame-101": 
			launch101();
			break;
		case "frame-102": 
			launch102();
			break;
		case "frame-103": 
			launch103();
			break;
		case "frame-104": 
			launch104();
			break;
		case "frame-105": 
			launch105();
			break;
		case "frame-106": 
			launch106();
			break;
		case "frame-107": 
			launch107();
			break;
		case "frame-108": 
			launch108();
			break;
		case "frame-109": 
			launch109();
			break;
		case "frame-110": 
			launch110();
			break;
		case "frame-111": 
			launch111();
			break;
		case "frame-112": 
			launch112();
			break;
		case "frame-113": 
			launch113();
			break;
		case "frame-113-a": 
			launch113a();
			break;
		case "frame-114": 
			launch114();
			break;
		case "frame-115": 
			launch115();
			break;
		case "frame-116": 
			launch116();
			break;
		case "frame-301": 
			launch301();
			break;
	}
}

var initMenuButtons = function(){
	links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	initMenuButtons();
	//hideEverythingBut($("#pre-load"));
	hideEverythingBut($("#frame-000"));
	//loadImages();
};

$(document).ready(main);